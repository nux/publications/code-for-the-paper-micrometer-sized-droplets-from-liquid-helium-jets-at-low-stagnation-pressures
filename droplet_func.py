import numpy as np
import cv2
import math as math
from statistics import mean 
import os
from decimal import Decimal
import csv
from scipy import stats
import matplotlib.pyplot as plt


def fill_droplets(img, h):
    """This function fills the droplets of the image. this helps the following code to be better at correctly identifying contours. 

    Args:
      img = the image that should be considered 
        h = the folder number

    Returns:
        the filled droplets in the original binary threshold image.
    """
    # Set the Threshold of imgage
    # For second argument set values equal to or above 95 to 0.
    # For third argument set values below 95 to 255.
    th, im_th = cv2.threshold(img, 95, 255, cv2.THRESH_BINARY_INV)
    
    
    # Copy the thresholded image.
    im_th = cv2.cvtColor(im_th, cv2.COLOR_BGR2GRAY)
    im_floodfill = im_th.copy()

    #in the first image folder the nozzle is visible on the left side of the shadowgraphy images.
    #Therefore we cut the outermost left pixles of the image by setting them to black. 
    if h == 1:
        im_floodfill[:,:50]=0
    
    # Mask used to flood filling.
    # Notice the size needs to be 2 pixels larger than the image.
    height, w = im_th.shape[:2]
    mask = np.zeros((height+2, w+2), np.uint8)
    
    # Floodfill from point (0, 0)
    cv2.floodFill(im_floodfill, mask, (0,0), 225);
    # Invert floodfilled image
    im_floodfill_inv = cv2.bitwise_not(im_floodfill)
    # Combine the two images to get the foreground.
    im_out = im_th | im_floodfill_inv
    #invert the picture
    img_filled = cv2.bitwise_not(im_out)
       
    return img_filled

def make_contours(img, img_filled):
    """This function draws contours onto the inserted img with the shapes taken from img_filled. 
    Args:
        img = the image that should be considered 
        img_filled = the image that should be considered (img) with filled droplets

    Returns:
        the image with the contours
    """    
    contours,_ = cv2.findContours(img_filled, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
    
    center_array = []    
    im = img.copy()
    
    for cnt in contours:
        approx = cv2.approxPolyDP(cnt, 0.0001*cv2.arcLength(cnt, True), True)
        
        # compute the center of the contour
        M = cv2.moments(cnt)
        
        if M["m00"] != 0:
            cX = int(M["m10"] / M["m00"])
            cY = int(M["m01"] / M["m00"])
            
        else: 
            cX, cY = 0, 0    
        
        
        center_array.append([cX, cY])

        #calculate area and perimeter to get rid of large countours
        area = cv2.contourArea(cnt)
        peri = cv2.arcLength(cnt, True)          
        
        #calculates rectangle around the contours
        #x and y give the top-left coordinate
        #w and h give the width an the height
        x,y,w,h = cv2.boundingRect(cnt)
        
        #setting the range of the allowed perimeter of droplets
        #if nomaximum is set, then the whole picture would be recognized as droplet, which is not true. 
        if peri <= 2000:
            #setting the minimal area of pixels a droplet has in order to get rid of artefacts of bad images.
            if area >= 1:
                
                cv2.drawContours(im, [cnt], 0, (0,0,225), 1)

    return im
                   
def make_ellipse(img, img_filled):
    
    """This function draws ellipses onto the inserted img with the shapes taken from img_filled. 
        Note that the ellipse contours of the droplets are able to rotate according to the directioning of the droplets 
        and thus the aspect ratio is rather precise.

    Args:
        img = the image that should be considered 
        img_filled = the image that should be considered (img) with filled droplets

    Returns:
        the image with the ellipses and the average of the calculated elliptical aspect ratios
        of the found ellipse contours.
    """    
    original_height, original_width = img.shape[:2]

    contours, _ = cv2.findContours(img_filled, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    
    i = 1
    center_array = []
    ar_ellipse_array =[]
    
    Minor = []
    Major = []
    Angle = []
    
    im = img.copy()
    
    for cnt in contours:
        
        #approx takes an poly approximation of the contour cnt
        approx = cv2.approxPolyDP(cnt, 0.0001*cv2.arcLength(cnt, True), True)

        # compute the center of the contour
        M = cv2.moments(cnt)
        
        if M["m00"] != 0:
            cX = int(M["m10"] / M["m00"])
            cY = int(M["m01"] / M["m00"])
            
        else: 
            cX, cY = 0, 0    
        
        #calculate area and perimeter to get rid of large countours
        area = cv2.contourArea(cnt)
        peri = cv2.arcLength(cnt, True)
        
        #more than 5 data points are needed for an ellipse to be found...
        if cnt.shape[0] >= 5:      
            
            #make ellipses
            #(x, y), (Major axis, minor axis), angle = cv2.fitEllipse(cnt)
            ellipse = cv2.fitEllipse(cnt)

            #area of ellipse
            major_value = ellipse[1][0]
            minor_value = ellipse[1][1]
            
            angle_value = ellipse[2]
            
            if major_value > 0: 
    
                Area_ellipse = math.pi*major_value*minor_value/4
                Aspect_ratio_ellipse = minor_value / major_value
                #delta value indicates the difference of droplet area to ellipse area. Hence, it is a measure of how well the elliptical approximation fits. 
                delta = abs(Area_ellipse-area)
        
            '''due to the fact that the y-axis of the picture is axis 0, the Major value goes in this direction. 
            Analog the x-axis of the picture is the axis 1 of the computer translation. 
            Thus, the Minor value goes in this direction. This implies that for an ellipse streched along x-axis, 
            the calculated Minor value is larger than the major value. Followingly, we nee to divide the Minor (larger) value
            by the Major (smaller) value to obtain the aspect ratio correctly according to the alignment of our picture. '''
            
            
            #draw ellipses
            #setting the range of the allowed area of droplets. 
            #if no maximum is set, then the whole picture would be recognized as droplet, which is not true.
            #if no minimum is set for the area of pixels a droplet has artefacts (coming from bad imaging) would be recognized as droplets
            #the delta value has an upper limit to secure the elliptical approximation of the droplet to be sensible.
            if 20 < area < 1000 and delta < 10:
                cv2.ellipse(im,ellipse,(0,255,0),1)
                ar_ellipse_array.append(Aspect_ratio_ellipse)   
                Major.append(major_value)
                Minor.append(minor_value)
                Angle.append(angle_value)

                #distance from the left corner is equal to cX
                center_array.append([cX, cY])

    ar_ellipse_array = np.array(ar_ellipse_array)
    center_array = np.array(center_array)
    
    #make numpy arrays
    Major = np.array(Major)
    Minor = np.array(Minor)
    Angle = np.array(Angle)
    
    if len(Angle) == 0 and len(ar_ellipse_array) == 0 and len(Minor) == 0 and len(Major) == 0:
        return None
    
    
    av_angle = np.mean(Angle)
    #calculate mean value of aspect ratios of rectangles for this picture
    av_ar_ellipse = np.mean(ar_ellipse_array)
    
    # First we take a float and convert it to a decimal
    av_ar_ellipse_rounded = Decimal(av_ar_ellipse)

    # Then we round it to 2 places
    av_ar_ellipse_rounded = round(av_ar_ellipse_rounded,2) 
    return im, Minor, Major,av_ar_ellipse,center_array, ar_ellipse_array, Angle , av_angle


def droplet_recognition_test(*, folder_number_min, folder_number_max, image_number_min, image_number_max, all_filenames, input_path, output_path, saving=False):
    """This function shows the shadowgraphy images with drawn, labeled and calculated droplets and prints the file number
    Args:
        folder_number_min = lowest number of folder that should be considered
        folder_number_max = highest number of folder that should be considered+1
        image_number_min = lowest number of image that should be considered
        image_number_max = highest number of image that should be considered+1
        all_filename = name repository for all folders and all images
        input_path = path of image location
        output_path = path for output of function
        saving = bool, whether you want to save

    Returns:
        0    """ 

    counter = 0
    droplet_count = 0
    
    
    for h in range(folder_number_min, folder_number_max):
        for i in range(image_number_min,image_number_max):
            path = all_filenames[h-1]
            # print the name of the images we chose above in this cell. 
            print(path[i-1])
            img = cv2.imread(input_path+str(h)+'/'+str(path[i-1]), cv2.IMREAD_COLOR)        
            img_filled = fill_droplets(img, h)
            
            # in the first image folder the nozzle is visible on the left side of the shadowgraphy images.
            # Therefore we cut the outermost left pixles of the image by setting them to black. 
            if h == 1:
                img_filled[:,:50] = 0
        
            processed_image_c = make_contours(img, img_filled)
            output_tuple = make_ellipse(img, img_filled)
            if output_tuple is None:
                counter += 1
                continue
                
            processed_image_ell, Minor, Major, av_ell, zentrum, ar, angle, av_angle = output_tuple
            droplet_count += len(Minor)
            
            # open the image(s) with drawn droplet contours
            show_image(processed_image_c, h, i, output_path + 'image_w_contour_folder_'+str(h)+'_image_'+str(i), saving)
            # open the image(s) with drawn ellipses approximating the droplets
            show_image(processed_image_ell, h, i, output_path + 'image_w_ellipse_folder_'+str(h)+'_image_'+str(i), saving)
            
    print('skipped images:', counter)
    print('Considered Droplets in total:', droplet_count)


def show_image(image, folder_id, image_id, path, saving=False):
    ''' '''
    """This function shows the image chosen and saves it, if requested. 

    Args:
        image = the image that should be considered 
        folder_id = the folder of the image considered
        image_id = number of image considered
        path = path where you want to save your image to; please include name of the target file
        saving = bool, whether you want to save

    Returns:
        0 """    
    cv2.imshow('image ' + str(image_id) + 'in folder ' + str(folder_id), image)
    if saving:
        cv2.imwrite(path + '.png', image)
    cv2.waitKey(0) 
    cv2.destroyAllWindows()
    return 0




def export_data(*, data, folder_number_min, folder_number_max, name, path):
    """Exports data as an excelfil. The first column ofthe excel file gives usthe file number (minus 1) and the 
        second column gives the values of the data for all considered droplets. 
    Args:
        data = considered data
        folder_number_min = lowest number of folder that should be considered
        folder_number_max = highest number of folder that should be considered+1
        name = name of excel file
        path = output path
    Returns:
        0    """ 

    array_export = []

    for i in range(folder_number_min-1, folder_number_max-1):
        for j in range(0, len(data[i])):
            array_export.append([i, data[i][j]])

    with open((path) + 'data for ' + (name) +".csv", "w", newline="") as csvfile:
        writer = csv.writer(csvfile, delimiter=",", quotechar="\"", quoting=csv.QUOTE_MINIMAL)
        writer.writerow(("Number of Folder", name))
        for row in array_export:
            writer.writerow(row)
    csvfile.close()     


def import_data(*, path_name, folder_number_min, folder_number_max):
    """Imports csv data into the code. The function is only necessary if you did not newly generate the data.
    Args:
        data = considered data
        folder_number_min = lowest number of folder that should be considered
        folder_number_max = highest number of folder that should be considered+1
        name = name of excel file
        path = output path

    Returns:
        0    """ 
    
    
    array_import = np.genfromtxt(path_name,delimiter=',')
    data_array_import = [[] for _ in range(folder_number_max-1)]
    
    for i in range(folder_number_min-1, folder_number_max-1):
        for j in range(0, len(array_import)):
            if i == array_import[j][0]:
                data_array_import[i].append(array_import[j][1])
    return data_array_import


def plot_boxplot(*, folder_number_min, folder_number_max, z_positions, data, mean, data_choice, path, saving = False):
    """This function shows the shadowgraphy images with drawn, labeled and calculated droplets and prints the file number
    Args:
        folder_number_min = lowest number of folder that should be considered
        folder_number_max = highest number of folder that should be considered+1
        z_positions = z position where the images were taken
        data = data to be considered for the boxplot
        mean = mean per z position of data to be considered 
        data_choice = choose either "aspect ratio" or "average radius" depending on the type of data you want to consider
        path = path for output of function
        saving = bool, whether you want to save

    Returns:
        0    """ 

    # Create a figure instance for the boxplot of the aspect ratio data
    fig = plt.figure(figsize=(7,4))
    # Create an axes instance
    ax = fig.add_subplot(111)
    
    # the positions of the chosen folders
    z_value = z_positions[folder_number_min-1:folder_number_max-1]
    
    # plot the boxplot
    bp = ax.boxplot(data, widths = 0.1, patch_artist=False, notch = False, showfliers=False,
                    positions=z_value)
    
    # change color and linewidth of the whiskers
    for whisker in bp['whiskers']:
        whisker.set(color='black', linewidth=2)
        
    # change the style of fliers and their fill
    for flier in bp['fliers']:
        flier.set(marker='o', color='green', alpha=0.5)
    
    # getting the positions of whiskers fliers and medians
    whis = [item.get_ydata() for item in bp['whiskers']]
    #print('Whiskers', whis)
    flier = [item.get_ydata() for item in bp['fliers']]
    #print('Fliers', (flier))
    median = [item.get_ydata() for item in bp['medians']]
    #print('Median', median)
    
    
    plt.grid()
    plt.xlabel('Downstream position [mm]')
    plt.xlim(0,104)
    ax.set_xticklabels(z_value)

    if data_choice == "aspect ratio":
        # setting the axis configuration
        plt.ylim(0.9, 1.9)
        plt.ylabel('Average aspect ratio of all droplets')  
        
        # change outline color, fill color and linewidth of the boxes
        for box in bp['boxes']:
            # change outline color
            box.set( color='grey', linewidth=2)
        box.set(label='Box of Aspect Ratio')

        # change color and linewidth of the caps
        for cap in bp['caps']:
            cap.set(color='black', linewidth=2)
        cap.set(label='Whisker of Aspect Ratio')
    
        # change color and linewidth of the medians
        for median in bp['medians']:
            median.set(color='orange', linewidth=2)
        median.set(label='Median of Aspect Ratio')
        
        # plotting additionally the mean of the aspect ratio
        plt.plot(z_value, (mean), 'bo',label='Mean of Aspect Ratio')
        plt.legend()
        if saving:
            plt.savefig( path + 'aspect_ratio_boxplots_vs_zposition.pdf')
    
    if data_choice == "average radius":
        plt.ylim(2.5,18.5)
        plt.ylabel('Average radius of all droplets [' + u"\u03bcm" + ']') 
                
        # change outline color, fill color and linewidth of the boxes
        for box in bp['boxes']:
            # change outline color
            box.set( color='grey', linewidth=2)
        box.set(label='Box of Average Radius')
                    
        # change color and linewidth of the caps
        for cap in bp['caps']:
            cap.set(color='black', linewidth=2)
        cap.set(label='Whisker of Average Radius')
        
        # change color and linewidth of the medians
        for median in bp['medians']:
            median.set(color='orange', linewidth=2)
        median.set(label='Median of Average Radius')      
        
        plt.plot(z_value, mean, 'bo',label='Mean of Average Radius')
        plt.legend()
        if saving:
            plt.savefig( path + 'average_radius_boxplots.pdf')
    

def aspect_ratio_histogram(*, folder_number_min, folder_number_max, image_number_min, image_number_max, all_filenames, input_path, output_path, saving=False):
    """This function shows the histogram of aspect ratio for considered droplets of the chosen images

    Args:
        folder_number_min = lowest number of folder that should be considered
        folder_number_max = highest number of folder that should be considered+1
        image_number_min = lowest number of image that should be considered
        image_number_max = highest number of image that should be considered+1
        all_filenames = name repository for all folders and all images
        input_path = path of image location
        output_path = path for output of function
        saving = bool, whether you want to save

    Returns:
        0    """ 

    counter = 0
    droplet_count = 0
    Standard_dev = []
    mean_ar = []
    all_AR = []
    
    for h in range(folder_number_min, folder_number_max):
        all_ar_means = []
        all_ar = []
        for i in range(image_number_min,image_number_max):
            path = all_filenames[h-1]
            img = cv2.imread(input_path+str(h)+'/'+str(path[i-1]), cv2.IMREAD_COLOR) 
            img_filled = fill_droplets(img, h)
            
            # in the first image folder the nozzle is visible on the left side of the shadowgraphy images.
            # Therefore we cut the outermost left pixles of the image by setting them to black. 
            if h == 1:
                img_filled[:,:50] = 0
            
            output_tuple = make_ellipse(img, img_filled)
            if output_tuple is None:
                counter += 1
                continue
                
            processed_image_2, Minor, Major, av_ell, zentrum, ar, angle, av_angle = output_tuple
            
            droplet_count += len(Minor)
            all_ar.extend(ar)
            all_ar_means.append(av_ell)
                
        all_AR.append(all_ar)
    
        # plotting all ellipses as histograms
        plt.figure()
        bins = np.arange(0.9,2.0,0.005)
        bin_height, bin_position, patches = plt.hist(all_ar,bins,color='g')
        plt.xlabel('Aspect ratio per droplet at position number '+str(h))
        plt.ylabel('Counts')
        
        y_value = bin_height
        x_value = []
        for k in range(0,len(bin_height)):
                x_value.append(bin_position[k])
        x_value = np.array(x_value)        
        
        mean_ar.append(np.mean(all_ar))
        Standard_dev.append( np.std(all_ar))
        plt.grid()
        if saving:
            plt.savefig(output_path + 'Histogram_aspect_ratio_of_considered_droplets_in_folder_'+str(h)+'.pdf')  
        
    print('Skipped images in total:', counter) 
    print('Mean AR_per folder:', mean_ar)
    print('Standard Deviation:', Standard_dev)
    print('Considered Droplets in total:', droplet_count)    

    return all_AR, mean_ar


def mean_over_position(*, folder_number_min, folder_number_max, z_positions, mean_data, data_choice, path, saving = False):
    """Creates a figure for the mean values of the chosen data over the z positions

    Args:
        folder_number_min = lowest number of folder that should be considered
        folder_number_max = highest number of folder that should be considered+1
        z_positions = z position where the images were taken
        mean_data = mean per z position of data to be considered 
        data_choice = choose either "aspect ratio" or "average radius" depending on the type of data you want to consider
        path = path for output of function
        saving = bool, whether you want to save
        
    Returns:
        0    """ 
    # Create a figure instance for the mean values of the chosen data
    fig = plt.figure(figsize=(10,8))

    # Create an axes instance
    ax = fig.add_subplot(111)

    # the positions of the chosen folders
    z_value = z_positions[folder_number_min-1:folder_number_max-1]
    
    if data_choice == "aspect ratio":
        plt.ylim(0.8,1.8)
        plt.ylabel('Average aspect ratio of all droplets')
        plt.plot(z_value, mean_data, 'bo',label='Exact mean of aspect ratio')   
        if saving:
            plt.savefig(path + 'mean_aspect_ratio_vs_zposition.pdf')
            
    if data_choice == "average radius":
        plt.ylim(0,14)
        plt.ylabel('Average radius of all droplets [' + u"\u03bcm" + ']')
        plt.plot(z_value, mean_data, 'bo',label='Exact mean of average radius')
        if saving:
            plt.savefig(path + 'mean_average_radius_vs_zposition.pdf')
    
    plt.xlim(0,104)
    plt.xlabel('Downstream position [mm]')
    plt.legend()
    plt.grid()


def mean_radius_histogram(*, folder_number_min, folder_number_max, image_number_min, image_number_max, all_filenames, input_path, output_path, saving=False):
    """This function shows the histogram of mean radius of considered droplets of the chosen images

    Args:
        folder_number_min = lowest number of folder that should be considered
        folder_number_max = highest number of folder that should be considered+1
        image_number_min = lowest number of image that should be considered
        image_number_max = highest number of image that should be considered+1
        all_filenames = name repository for all folders and all images
        input_path = path of image location
        output_path = path for output of function
        saving = bool, whether you want to save

    Returns:
        0    """ 

    # calculating mean radius of droplets
    MR = []
    counter_img_tot = 0
    Deviation = []
    counter_droplets = 0
    mr_between = [[] for _ in range(folder_number_max-1)]
    
    for h in range(folder_number_min, folder_number_max):
        Minor_all = []
        Major_all = []
        counter_img = 0
        for i in range(image_number_min,image_number_max):
            path = all_filenames[h-1]
            img = cv2.imread(input_path+str(h)+'/'+str(path[i-1]), cv2.IMREAD_COLOR)
            img_filled = fill_droplets(img, h)
            
            # in the first image folder the nozzle is visible on the left side of the shadowgraphy images.
            # Therefore we cut the outermost left pixles of the image by setting them to black. 
            if h == 1:
                img_filled[:,:50]=0
                
            output_tuple = make_ellipse(img, img_filled)
            if output_tuple is None:
                counter_img_tot += 1
                counter_img += 1
                continue
                
            processed_image_2, Minor, Major, av_ell, zentrum, ar, angle, av_angle = output_tuple
            
            Minor_all.extend(Minor)
            Major_all.extend(Major)
    
        for i in range(len(Minor_all)):
            x = (Minor_all[i]*0.5)+(Major_all[i]*0.5)
            x /= 2
            x *= 1.45
            mr_between[h-1].append(x)
        
        if len(mr_between[h-1]) == 0:
            mr_between[h-1].append(0)
            
        MR.append(mean(mr_between[h-1]))
        Deviation.append(np.std(mr_between[h-1]))
        counter_droplets += len(Minor_all)
        
        #print('Number of considered droplets in folder '+str(h)+':', len(Minor_all))
        #print('skipped images folder '+str(h)+':', counter_img)
        
        # plotting histograms of mean radius for all found droplets 
        plt.figure()
        bins = np.arange(0,15,0.1)
        bin_height, bin_position, patches = plt.hist(mr_between[h-1],bins,color='g')
        plt.xlabel('Average radius per droplet at position number '+str(h)+' ['+ u"\u03bcm" + ']')
        plt.ylabel('Counts')
        plt.grid()
        if saving:
            plt.savefig(output_path + 'Histogram_average_radius_of_considered_droplets_in_folder_'+str(h)+'.pdf')
        
    print('Mean radius averaged per folder', MR)
    print('Standard Deviation each folder', Deviation)
    print('skipped images in total:', counter_img_tot)
    print('Droplets considered in total:', counter_droplets)

    return mr_between, MR

